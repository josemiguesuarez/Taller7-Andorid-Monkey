# Monkey Adhoc

## Instalación
1. Instalar Java.
1. Descargar o clonar el repositorio. En la raíz del proyecto encontrará el archivo "monkey.jar". Este archivo es lo único que necesita para ejecutar el proyecto. 
1. Instalar el SDK de Android, en particular se debe tener instalado Android Debug Bridge (ADB) 
1. Ejecutar un emulador de Andorid. Se recomienda el dispositivo Nexus 5. 

## Ejemplos
1. Monkey simple para hacer clic sobre la pantalla y rotar el dispositivo
```
java -jar monkey.jar -adb /usr/local/bin/ -port 5554 -token Uu8Z/LvhuqeBSuGm -tap -rotate
```

2. Especificar la semilla con la opción "-s" y definir la cantidad de enventos con la opción "-e".
```
java -jar monkey.jar -s 89 -e 200 -adb /usr/local/bin/ -port 5554 -token Uu8Z/LvhuqeBSuGm -tap -rotate
```

3. Especificar la aplicación que se desea instalar y someter a la prueba.
```
java -jar monkey.jar -adb /usr/local/bin/ -port 5554 -token Uu8Z/LvhuqeBSuGm -apk "Wikipedia.apk" -package org.wikipedia -tap -rotate
```

4. Se especifica la probabilidad de ocurrencia de algunos eventos agregando un número entre 0 y 100 al lado de cada tipo de comando. (i.e. -text 60, singnifica que con un 60% de probabilidad se ejecutará el comando "text" cuando pseudo-aleatoriamente sea seleccionado. )
```
java -jar monkey.jar -adb /usr/local/bin/ -port 5554 -token Uu8Z/LvhuqeBSuGm -tap 90 -rotate 10 -text 60
```
5. Ejecutar todos los comandos 
```
java -jar monkey.jar -adb /usr/local/bin/ -port 5554 -token Uu8Z/LvhuqeBSuGm -tap -text -swipe -key -rotate -net -acc -light
```

## Documentación


| Argumento         | Opción  | Posibles valores | Descripción                                                                                                             |
| ----------------- | ------- | ---------------- | ----------------------------------------------------------------------------------------------------------------------- |
| ADB Root          | -adb    | string           | Por defecto es /usr/local/bin/                                                                                          |
| Emulator Port     | -port   | string           | Ejecutar "adb devices" en la carpeta donde está instalado ADB para saber el puerto del dispositivo. Por defecto es 5554 |
| Telnet token      | -token  | string           | Ejecutar "telnet localhost \<Emulator Port\>". En la consola aparecerá el archivo donde se encuentra el token.          |
| APK               | -apk    | string           | Ruta del apk. Si no se especifica la ruta, el programa ejecutará los comandos sobre la pantalla que esté activa.        |
| Número de eventos | -e      | int >0           | Número de eventos que se quieren ejecutar. Por defecto son 10.                                                          |
| Semilla           | -s      | int >0           | Semilla para generar los eventos pseudo-aleatorios.                                                                     |
| Tap               | -tap    | int p,  0<p< 100 | Hace clic sobre la pantalla.                                                                                            |
| Text              | -text   | int p,  0<p< 100 | Escribe una palabra con tamaño entre 1 y 10 caracteres.                                                                 |
| Swipe             | -swipe  | int p,  0<p< 100 | Simula un deslizamiento sobre la pantalla de un punto A a un punto B.                                                   |
| KeyEvent          | -key    | int p,  0<p< 100 | Presiona una tecla física del dispositivo, inlcuye teclas del teclado.                                                  |
| Rotate            | -rotate | int p,  0<p< 100 | Gira el dispositivo                                                                                                     |
| Network           | -net    | int p,  0<p< 100 | Simula cambios en la velocidad de conexión a internet.                                                                  |
| Acceleration      | -acc    | int p,  0<p< 100 | Simula la aceleración del dispositivo en los 3 ejes.                                                                    |
| Light             | -light  | int p,  0<p< 100 | Simula cambios en el sensor de luz del dispositivo.                                                                     |

