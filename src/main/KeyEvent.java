package main;

import java.util.Random;

public class KeyEvent extends Command {
	public static final int MAX_KEY_EVENT=283;
	
	public KeyEvent(Random random, int probability) {
		super("KeyEvent", random, probability);
		// TODO Auto-generated constructor stub
	}


	@Override
	protected void exec() {
		int x = random.nextInt(MAX_KEY_EVENT);
		Main.execADBInput("keyevent " + x );
	}

}
