package main;
import java.util.Random;

public class Tap extends Command {

	public Tap( Random random, int probability) {
		super("Tap", random, probability);
	}

	@Override
	protected void exec() {
		int x = random.nextInt(Main.MAX_X);
		int y = random.nextInt(Main.MAX_Y);
		Main.execADBInput("tap " + x + " " + y);
	}

}
