package main;

import java.util.Random;

public class Swipe extends Command {

	public Swipe( Random random, int probability) {
		super("Swipe", random, probability);
	}

	@Override
	protected void exec() {
		int x0 = random.nextInt(Main.MAX_X);
		int y0 = random.nextInt(Main.MAX_Y);
		int x1 = random.nextInt(Main.MAX_X);
		int y1 = random.nextInt(Main.MAX_Y);
		Main.execADBInput("swipe " + x0 + " " + y0 + " " + x1 + " " + y1);
	}

}
