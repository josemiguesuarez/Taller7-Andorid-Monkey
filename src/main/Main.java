package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;


public class Main {


	public static String adbRoot;
	public static final String ADB_INPUT = "adb shell input ";
	public static String telnetToken;
	public static String emulatorPort;
	public static final int MAX_X = 1080;
	public static final int MAX_Y = 1920;
	private Random random;
	private Command[] commands;
	private long seed;

	private static BufferedWriter connectToTelnet() throws IOException {
		Runtime rt = Runtime.getRuntime();
		Process telnet = rt.exec("telnet localhost "+emulatorPort);
		return new BufferedWriter(new OutputStreamWriter(telnet.getOutputStream()));
	}

	public static void execADBInput (String command) {
		System.out.println("ADB command: " +adbRoot + ADB_INPUT + command );
		execADB(ADB_INPUT + command);
	}

	public static void execADB (String command) {
		System.out.println("ADB command: " +adbRoot + command );
		Runtime rt = Runtime.getRuntime();
		try {
			Process result = rt.exec(adbRoot + command);
			BufferedReader error = new BufferedReader(new InputStreamReader(result.getErrorStream()));
			String line;
			while((line = error.readLine()) != null){
				System.out.println(line);
			}
			error.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void execTelNet (String command) {
		System.out.println("TelNet command: " +command );
		try {
			BufferedWriter out = connectToTelnet();
			out.write("auth "+telnetToken+"\n");
			out.write(command + "\n");
			out.write("quit\n");
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		try {
			Map <String, String> map = readArgs(args);


			Main main = new Main(map);
			int events = 10;
			if(args.length == 0){
				Scanner scanner = new Scanner(System.in);
				System.out.println("Número de eventos: ");
				events = scanner.nextInt();
				main.defineProbabilities(scanner);
				scanner.close();
			} else {
				events = getInt(map, "-e", 10, 10);
			}
			main.exec(events);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Main (Map <String, String> argsMap) throws Exception {
		seed =getLong(argsMap, "-s", System.nanoTime());
		emulatorPort = getString(argsMap, "-port", "5554");
		adbRoot = getString(argsMap, "-adb", "/usr/local/bin/");
		telnetToken = getString(argsMap, "-token", "Uu8Z/LvhuqeBSuGm");
		String apkPath = getString(argsMap, "-apk", null);
		String packageName = getString(argsMap, "-package", null);
		if(apkPath!= null){
			if(packageName == null){
				throw new Exception ("Cuando se especifica la opción -apk, también se debe especificar la opción -package");
			}
			execADB("adb uninstall " + packageName);
			execADB("adb install " + apkPath );
			execADB("adb shell monkey -p " + packageName +" 1 " + apkPath);
		}

		random = new Random(seed);
		commands = new Command[8];
		commands[0] = new Tap(random, getInt(argsMap, "-tap", 100, 0));
		commands[1] = new Text(random, getInt(argsMap, "-text", 100, 0));
		commands[2] = new Swipe(random, getInt(argsMap, "-swipe", 100, 0));
		commands[3] = new KeyEvent(random, getInt(argsMap, "-key",100,  0));
		commands[4] = new Rotate(random, getInt(argsMap, "-rotate", 100, 0));
		commands[5] = new Network(random, getInt(argsMap, "-net",100,  0));
		commands[6] = new Acceleration(random, getInt(argsMap, "-acc", 100, 0));
		commands[7] = new Light(random, getInt(argsMap, "-light", 100, 0));

		if(!atLeastOne()) throw new Exception("Se debe definir al menos un tipo de evento para ejecutar.");
	}

	private boolean atLeastOne() {
		for(Command com: commands){
			if(com.getProbability()>0)
				return true;
		}
		return false;

	}

	public static int getInt(Map <String, String> argsMap, String option, int defaultInt, int ifNotExist){

		if(!argsMap.containsKey(option))
			return ifNotExist;
		String value = argsMap.get(option);		
		if(value == null){
			return defaultInt;
		} else {
			try{
				return Integer.parseInt(value);
			}catch(Exception e){
				throw new IllegalArgumentException("Option" + option + " should be integer.");
			}
		}
	}
	public static long getLong(Map <String, String> argsMap, String option, long defaultLong){
		String value = argsMap.get(option);
		if(value == null){
			return defaultLong;
		} else {
			try{
				return Long.parseLong(value);
			}catch(Exception e){
				throw new IllegalArgumentException("Option" + option + " should be a long.");
			}
		}
	}

	public static String getString(Map <String, String> argsMap, String option, String defaultString){
		String value = argsMap.get(option);
		if(value == null)
			return defaultString;
		else 
			return value;

	}

	public void defineProbabilities (Scanner scanner) {
		for(Command command: commands) {
			System.out.println("Probability of " + command.getName() + " (from 0 to 100): " );
			int probability = scanner.nextInt();
			command.setProbability(probability);
		}
	}

	public static Map<String, String> readArgs (String[] args){

		Map<String, String> map = new HashMap<String, String>();

		for (int i = 0; i < args.length; i++) {
			String argument = args[i];
			String nextArg =null;
			if(i < args.length-1){
				if(args[i+1].charAt(0) != '-'){
					nextArg = args[++i];
				}
			}
			if(argument.charAt(0) != '-')
				throw new IllegalArgumentException("Not a valid argument: "+argument + ". All options should start with -.");
			if (argument.length() < 2)
				throw new IllegalArgumentException("Not a valid argument: "+argument);

			map.put(argument, nextArg);
		}
		return map;

	}

	public void exec(int events) throws InterruptedException {
		for(int i = 0;i < events; i++) {
			int randomInt = random.nextInt(commands.length);
			boolean wasExecuted = commands[randomInt].execByDistribution();
			if(wasExecuted)
				Thread.sleep(1000);
			else
				i--;
		}
		System.out.println("Final de la ejecución con la semilla: " + seed);
	}


}
