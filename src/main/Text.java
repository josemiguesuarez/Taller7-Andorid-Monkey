package main;

import java.util.Random;

public class Text extends Command {

	public Text( Random random, int probability) {
		super("Text input", random, probability);
	}

	@Override
	protected void exec() {
		// TODO Auto-generated method stub
		Main.execADBInput("text " + randomString());
	}
	
	private String randomString() {
		StringBuilder sb = new StringBuilder();
		int length  = random.nextInt(9)+1;
		for(int i = 0; i < length; i++) {
			char c = (char)(random.nextInt(96)+32);
			sb.append(c);
		}
		return sb.toString();
	}

}
