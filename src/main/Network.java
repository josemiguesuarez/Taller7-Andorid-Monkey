package main;

import java.util.Random;

public class Network extends Command {
	public final static String [] SPEEDS = {
			"gsm" , 
			"hscsd" , 
			"gprs" , 
			"edge" , 
			"umts" , 
			"hsdpa" , 
			"lte" ,
			"evdo" , 
			"full"
	};
	public Network( Random random, int probability) {
		super("Network", random, probability);
	}

	@Override
	protected void exec() {
		int x = random.nextInt(SPEEDS.length);
		Main.execTelNet("network speed " + SPEEDS[x]);
	}

}
