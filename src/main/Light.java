package main;

import java.util.Random;

public class Light extends Command {

	public Light( Random random, int probability) {
		super("Light", random, probability);
	}

	@Override
	protected void exec() {
		int x = random.nextInt(40000);
		Main.execTelNet("sensor set light " + x );
	}

}
