package main;

import java.util.Random;

public abstract class Command {
	protected Random random;
	private int probability;
	private String name;

	public Command (String name, Random random, int probability) {
		this.name = name;
		this.random = random;
		this.probability = probability;
		System.out.println("Probability of " + name + ": " + probability);
	}
	public int getProbability() {
		return probability;
	}
	public void setProbability(int probability) {
		this.probability = probability;
	}
	public boolean execByDistribution() {
		boolean execute = random.nextInt(100) < probability;
		if(execute) {
			exec();
		}
		return execute;
	}
	protected abstract void exec();

	public String getName() {
		return name;
	}
}
