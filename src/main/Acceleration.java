package main;

import java.util.Random;

public class Acceleration extends Command{

	public Acceleration( Random random, int probability) {
		super("Acceleration", random, probability);
	}

	@Override
	protected void exec() {
		float x = 20*random.nextFloat()-10;
		float y = 20*random.nextFloat()-10;
		float z = 20*random.nextFloat()-10;
		Main.execTelNet("sensor set acceleration " + x + ":" + y + ":" + z);
		
	}

}
