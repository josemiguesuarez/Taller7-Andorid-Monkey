package main;

import java.util.Random;

public class Rotate extends Command {

	public Rotate( Random random, int probability) {
		super("Rotate", random, probability);
	}

	@Override
	protected void exec() {
		Main.execTelNet("rotate");
	}

}
